// Sample Season based on the 2v2 Round Robin we played.
// This is just to show what maintaining a season would look like.

object SampleSeason extends App {

  // Create players and establish a "league"
  // This can be added to while a season goes on with no harm done.
  val BalistcChilled = new Team("BalistcPanda", "ChilledAnarchy7")
  val CriasFang = new Team("Crias SK", "Fanghunter314")
  val DeadskyMeatball = new Team("i Deadsky i", "OGOPmeatb4lls")
  val SniperWrath = new Team("sniperfi0102", "TheWrathOfRhyno")
  val BlitzGruff = new Team("Unh0ly Blitz", "GruffTripod1354")
  val league = new Leaderboard(BalistcChilled, CriasFang, DeadskyMeatball, SniperWrath, BlitzGruff)

  // List of game results. As long as this stays in order, it can be added to.
  // Each time this file is run it recalculates ELOs from scratch, so changing the order will change the result.

  // Round One
  SniperWrath wins BalistcChilled
  CriasFang wins DeadskyMeatball

  // Round Two
  DeadskyMeatball wins BalistcChilled
  BlitzGruff wins SniperWrath

  // Round Three
  DeadskyMeatball wins BlitzGruff
  CriasFang wins BalistcChilled

  // Round Four
  CriasFang wins BlitzGruff
  SniperWrath wins DeadskyMeatball

  // Round Five
  CriasFang wins SniperWrath
  BlitzGruff wins BalistcChilled

  // RESULTS
  println(league)

  /* OUTPUT:

      1460		Crias SK / Fanghunter314
      1401		sniperfi0102 / TheWrathOfRhyno
      1400		i Deadsky i / OGOPmeatb4lls
      1400		Unh0ly Blitz / GruffTripod1354
      1339		BalistcPanda / ChilledAnarchy7

   */
}
