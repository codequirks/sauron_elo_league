class Leaderboard(teams: Team*) {
  override def toString: String = {
    teams.sortBy(_.rating * -1).map { t =>
      s"${t.rating}\t\t${t.playerOne} / ${t.playerTwo}"
    }.mkString("\n")
  }
}
