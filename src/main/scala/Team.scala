import com.github.esap120.scala_elo.KFactor.KFactor
import com.github.esap120.scala_elo.{KFactor, Matchup, Player}

class Team(val playerOne: String, val playerTwo: String) extends Player {
  override def wins(opponent: Player): Unit = {
    super.wins(opponent)
    this.updateRating(KFactor.USCF)
    opponent.updateRating(KFactor.USCF)
  }

  override def loses(opponent: Player): Unit = {
    super.loses(opponent)
    this.updateRating(KFactor.USCF)
    opponent.updateRating(KFactor.USCF)
  }

  override def draws(opponent: Player): Unit = {
    super.draws(opponent)
    this.updateRating(KFactor.USCF)
    opponent.updateRating(KFactor.USCF)
  }

  def updateRating(): Unit = super.updateRating(KFactor.USCF)

  override def toString: String = s"Team: $playerOne / $playerTwo\nGames Played: $gamesPlayed\nRating: $rating"
}
